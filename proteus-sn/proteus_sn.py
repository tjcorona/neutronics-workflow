#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
"""
Export script for Proteus-SN workflows
"""
import datetime
import logging
import os
import sys
sys.dont_write_bytecode = True

import smtk
import smtk.attribute
import smtk.io
import smtk.model

import rggsession

# Add the directory containing this file to the python module search list
import inspect
sys.path.insert(0, os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

from internal.writers import proteus_sn_writer

class Export(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "export Proteus-SN"

    def operateInternal(self):
        result = self.createResult(smtk.operation.Operation.Outcome.FAILED)

        try:
            success = ExportCMB(self.parameters(), self.log())
        except:
            smtk.ErrorMessage(self.log(), str(sys.exc_info()[0]))
            raise

        # Return with success
        if success:
            result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        return result

    def createSpecification(self):
        spec = self.createBaseSpecification()

        # Load export atts
        source_dir = os.path.abspath(os.path.dirname(__file__))
        sbt_path = os.path.join(source_dir, 'internal', 'proteus-sn-export.sbt')
        reader = smtk.io.AttributeReader()
        result = reader.read(spec, sbt_path, self.log())
        if result:
            print(self.log().convertToString(True))

        # opDef = spec.createDefinition('test op', 'operation')
        opDef = spec.findDefinition('proteus-sn-export')

        simulationDef = smtk.attribute.ResourceItemDefinition.New('attributes')
        simulationDef.setAcceptsEntries('smtk::attribute::Resource', '', True)
        opDef.addItemDefinition(simulationDef)

        modelDef = smtk.attribute.ResourceItemDefinition.New('model')
        modelDef.setAcceptsEntries('smtk::session::rgg::Resource', '', True)
        opDef.addItemDefinition(modelDef)

        resultDef = spec.createDefinition('result (' + self.name() + ')', 'result')

        return spec

ExportScope = type('ExportScope', (object,), dict())
# ---------------------------------------------------------------------
def ExportCMB(operator_spec, log):
    '''Entry function, called by CMB to write export files

    Returns boolean indicating success
    Parameters
    ----------
    spec: Top-level object passed in from CMB
    '''

    # Initialize scope instance to store spec values and other info
    scope = ExportScope()

    scope.logger = log
    scope.export_att = operator_spec
    scope.sim_atts = smtk.attribute.Resource.CastTo(operator_spec.find('attributes').value())
    if scope.sim_atts is None:
        msg = 'ERROR - No simlation attributes'
        print msg
        raise RuntimeError(msg)

    model_entity = smtk.model.Entity.CastTo(operator_spec.find('model').objectValue(0))
    model_resource = model_entity.modelResource()
    if model_resource is None:
        msg = 'ERROR - No model'
        print msg
        raise RuntimeError(msg)
    scope.model_entity = model_entity
    scope.model_manager = model_resource
    scope.model_path = model_resource.location()
    scope.model_file = os.path.basename(scope.model_path)

    # Get output filename (path)
    output_path = None
    dir_item = operator_spec.findDirectory('OutputDirectory')
    if dir_item is not None:
        output_path = dir_item.value(0)
    if not output_path:
        raise RuntimeError('No output directory specified')

    # Generate output directory if needed
    if not os.path.exists(output_path):
        print('Creating directory {}'.format(output_path))
        os.makedirs(output_path)
    scope.output_path = output_path

    reload(proteus_sn_writer)
    writer = proteus_sn_writer.ProteusSNWriter()
    success = writer.write(scope)

    return success
