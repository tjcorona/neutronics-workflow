<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">

  <Definitions>

    <AttDef Type="driver_input" BaseType="" Unique="true">

      <ItemDefinitions>

        <String Name="SN_TYPE" Label="Angular Cubature" NumberOfRequiredValues="1">
          <DiscreteInfo DefaultIndex="2">
            <Value Enum="Legendre (1-dimensional)">LEGENDRE 1-D</Value>
            <Value Enum="double Legendre (1-dimensional)">DOUBLE LEGENDRE</Value>
            <Value Enum="Carlson Even-Moment">CARLSON_EM</Value>
            <Value Enum="Carlson Level Symmetric">CARLSON_LM</Value>
            <Value Enum="Carlson Equal Weights">CARLSON_EQUALW</Value>
            <Value Enum="Legendre-Tchebychev">LEG-TCHEBY</Value>
            <Value Enum="X-direction Legendre-Tchebychev">X_DIR_LEG-TCHEBY</Value>
            <Value Enum="double Legendre-Tchebychev">DOUB_LEG-TCHEBY</Value>
            <Value Enum="Triangular Legendre-Tchebychev">TRI_LEG-TCHEBY</Value>
            <Value Enum="Lebedov-Laikov">LEBEDEV-LAIKOV</Value>
            <Value Enum="Corner-Based Tegmark Equal Weights">TEG_CORNER_EQWT</Value>
            <Value Enum="Corner-Based Tegmark Least Squares">TEG_CORNER_LSQ</Value>
            <Value Enum="Center-Based Tegmark Equal Weights">TEG_CENTROID_EQWT</Value>
            <Value Enum="Thurgood Triangulation">THURGOOD_TN</Value>
            <Value Enum="Thurgood Least Squares Fitted">THURGOOD_LEASTSQ</Value>
            <Value Enum="Even Order COBE Equal Weights">COBE_EVEN_EQWT</Value>
            <Value Enum="Even Order COBE Least Squares">COBE_EVEN_LEASTSQ</Value>
            <Value Enum="Odd Order COBE Equal Weights">COBE_ODD_EQWT</Value>
            <Value Enum="Odd Order COBE Least Squares">COBE_ODD_LEASTSQ</Value>
            <Value Enum="Carlson Pyramid">PYRAMID_CARLSON</Value>
            <Value Enum="Even Order COBE Sky Cube Equal Weights">COBE_C_EVEN_EQWT</Value>
            <Value Enum="Even Order COBE Sky Cube Least Squares">COBE_C_EVEN_LEASTSQ</Value>
            <Value Enum="Odd Order COBE Sky Cube Equal Weights">COBE_C_ODD_EQWT</Value>
            <Value Enum="Odd Order COBE Sky Cube Least Squares">COBE_C_ODD_LEASTSQ</Value>
          </DiscreteInfo>
        </String>

        <Int Name="THETA_RESOLUTION"
             Label="Polar Angle Resolution" NumberOfRequiredValues="1">
          <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
        </Int>

        <Int Name="PHI_RESOLUTION"
             Label="Azimuthal Angle Resolution" NumberOfRequiredValues="1">
          <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
              <DefaultValue>1</DefaultValue>
        </Int>

        <String Name="SOLVE_TYPE" Label="Solve Type" NumberOfRequiredValues="1">
          <DiscreteInfo DefaultIndex="2">
            <Value Enum="Forward">FORWARD</Value>
            <Value Enum="Adjoint">ADJOINT</Value>
            <Value Enum="Both">BOTH</Value>
          </DiscreteInfo>
        </String>

        <!--Instead of having a mesh file name input, we will bring
            our own mesh-->

        <File Name="SOURCEFILE_XS" Label="Cross Section File" ShouldExist="true">
          <BriefDescription>Name of decay chain text file</BriefDescription>
        </File>

        <File Name="SOURCEFILE_MATERIAL" Label="Material Mapping File" ShouldExist="true">
          <BriefDescription>Name of material mapping text file</BriefDescription>
        </File>

        <Group Name="AUXILIARY_INPUT_FILE" Label="Auxiliary Input File Options" AdvanceLevel="1">
          <ItemDefinitions>

            <File Name="SOURCEFILE_FIXEDSOURCE" Label="Fixed Source Input File"
                  ShouldExist="true" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Path to fixed source input file for a fixed source problem</BriefDescription>
            </File>

          </ItemDefinitions>
        </Group>

        <Group Name="DEBUG" Label="Debugging Options" AdvanceLevel="1">
          <ItemDefinitions>

            <Void Name="SKIPSOLVE" Label="Set Up But Do Not Solve"
                  Optional="true" IsEnabledByDefault="false"/>

            <Int Name="DEBUG_PRINT_LEVEL"
                 Label="Debug Printing Level for Entire Routine" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>0</DefaultValue>
            </Int>

            <Int Name="DEBUG_PRINT_SETUP"
                 Label="Debug Printing Level for Setup" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>0</DefaultValue>
            </Int>

            <Int Name="DEBUG_PRINT_FORMATION"
                 Label="Debug Printing Level for Matrix Formation" NumberOfRequiredValues="1">
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">10</Max>
              </RangeInfo>
              <DefaultValue>0</DefaultValue>
            </Int>

            <Int Name="DEBUG_PRINT_OUTER"
                 Label="Debug Printing Level for Outer Iterations" NumberOfRequiredValues="1">
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">10</Max>
              </RangeInfo>
              <DefaultValue>0</DefaultValue>
            </Int>

          </ItemDefinitions>
        </Group>

        <Group Name="PARALLELIZATION" Label="Parallelization Options" AdvanceLevel="1">
          <ItemDefinitions>

            <Int Name="SEGMENT_ANGLE" Label="# Processors (segments) in angle"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>0</DefaultValue>
              <BriefDescription>The number of segments to attempt in the angular approximation</BriefDescription>
              <DetailedDescription>
                0 = Code decides max parallelization
                1=Serial
                2 and up = # Processors (segments) in angle
              </DetailedDescription>
            </Int>

            <File Name="SOURCEFILE_MESHPART" Label="Mesh partitioning File"
                  ShouldExist="true" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Path to a precomputed partitioning mesh index file</BriefDescription>
            </File>

          </ItemDefinitions>
        </Group>

        <Group Name="ITERATIVE_SOLVER" Label="Iterative Solver Options" AdvanceLevel="1">
          <ItemDefinitions>

            <Double Name="EIGENVALUE_GUESS" Label="Initial eigenvalue guess"
                    NumberOfRequiredValues="1">
              <DefaultValue>1.0</DefaultValue>
            </Double>

            <Void Name="USE_TCHEBYCHEV_ACCEL" Label="Tchebychev acceleration of the fission source"
                  Optional="true" IsEnabledByDefault="true"/>

            <Int Name="ITERATIONS_FISSION"
                 Label="Maximum number of outer (fission source) iterations"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>100</DefaultValue>
            </Int>

            <Double Name="TOLERANCE_EIGENVALUE" Label="Relative error on the eigenvalue"
                    NumberOfRequiredValues="1">
              <DefaultValue>1.e-6</DefaultValue>
            </Double>

            <Double Name="TOLERANCE_FISSION" Label="Relative error on the fission source"
                    NumberOfRequiredValues="1">
              <DefaultValue>5.e-6</DefaultValue>
            </Double>

            <Double Name="TOLERANCE_FLUX" Label="Relative error on the flux solution"
                    NumberOfRequiredValues="1">
              <DefaultValue>1.e-7</DefaultValue>
            </Double>

            <Double Name="ITERATIVE_IMPROVEMENT" Label="Upper limit iterative improvement"
                    NumberOfRequiredValues="1">
              <DefaultValue>0.1</DefaultValue>
            </Double>

            <Int Name="ITERATIONS_MAXUPSCATTER"
                 Label="Maximum Gauss-Seidel 'upscatter' iterations per outer"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>5</DefaultValue>
            </Int>

            <Int Name="ITERATIONS_MINUPSCATTER"
                 Label="Minimum Gauss-Seidel 'upscatter' iterations per outer"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>5</DefaultValue>
            </Int>

            <Int Name="ITERATIONS_SCATTERING"
                 Label="Maximum number of source iterations per within group iteration"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>3</DefaultValue>
            </Int>

            <Int Name="ITERATIONS_SA_CG"
                 Label="Maximum number of within-group CG iterations"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>1000</DefaultValue>
            </Int>

            <Int Name="ITERATIONS_PETSC"
                 Label="Maximum number of CG iterations in PETSc preconditioner tree"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>1000</DefaultValue>
            </Int>

            <String Name="USE_PRECONDITIONER" Label="PETSc Preconditioner"
                    NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Symmetric successive over-relaxation (SSOR)">SSOR</Value>
                <Value Enum="Incomplete Cholesky factorization (ICC)">ICC</Value>
              </DiscreteInfo>
            </String>

            <Void Name="USE_DSA" Label="Synthetic diffusion acceleration to the source iteration"
                  Optional="true" IsEnabledByDefault="true"/>

            <Void Name="RELAX_SPATIAL" Label="Relax spatial matrix integration"
                  Optional="true" IsEnabledByDefault="true"/>

            <Double Name="USE_WEILANDT_SHIFT" Label="Weilandt shift"
                    NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
              <DetailedDescription>
                Apply a Weilandt shift to the system to accelerate convergence.
                Value must be greater than largest eigenvalue in system to maintain positive
                definite system.
              </DetailedDescription>
            </Double>

            <Void Name="BASIC_BWO" Label="Basic bandwith optimization"
                  Optional="true" IsEnabledByDefault="false"/>

          </ItemDefinitions>
        </Group>

        <Group Name="CROSS_SECTION" Label="Cross Section Options" AdvanceLevel="1">
          <ItemDefinitions>

            <String Name="USE_CSAPI" Label="Process cross sections"
                    NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="No">NO</Value>
                <Value Enum="Subgroup">SUBGROUP</Value>
                <Value Enum="Resonance Table">RESONANCETABLE</Value>
              </DiscreteInfo>
              <DetailedDescription>
                Indicates whether the subgroup or resonance table cross section library is to be
                used to generate cross sections. The cross section libraries are expected for
                heterogeneous geometry problems.
              </DetailedDescription>
            </String>

            <Void Name="USE_TRANSPORT_XS" Label="Use transport cross-section only"
                  Optional="true" IsEnabledByDefault="false"/>

            <Int Name="SCATTERING_ORDER"
                 Label="Legendre expansion order of the scattering kernel"
                 NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>0</DefaultValue>
            </Int>

            <Void Name="SCATTERING_CHANGETOTAL"
                  Label="Autocorrect scattering from spherical harmonic expansion"
                  Optional="true" IsEnabledByDefault="false"/>

            <String Name="SCATTERING_SETUPTYPE" Label="2L+1 factor"
                    NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Omitted">0</Value>
                <Value Enum="Multiplied">1</Value>
                <Value Enum="Divided">2</Value>
              </DiscreteInfo>
            </String>

          </ItemDefinitions>
        </Group>

        <Group Name="OUTPUT" Label="Output File Options" AdvanceLevel="1">
          <ItemDefinitions>

            <Int Name="MESHSIMULATE" Label="Export mesh in parallelized chunks"
                 NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>

            <Void Name="EXPORT_CUBATURE" Label="Export cubature to VTK file"
                  Optional="true" IsEnabledByDefault="false"/>

            <Void Name="EXPORT_XS_PRINT" Label="Print XS Data"
                  Optional="true" IsEnabledByDefault="false"/>

            <Void Name="EXPORT_MESH" Label="Export mesh to ASCII file"
                  Optional="true" IsEnabledByDefault="false"/>

            <Void Name="EXPORT_MESH_VTK" Label="Export mesh to VTK file"
                  Optional="true" IsEnabledByDefault="false"/>

            <Void Name="EXPORT_FLUX" Label="Export full solution"
                  Optional="true" IsEnabledByDefault="false"/>

            <File Name="EXPORT_FILE" Label="Output file" ShouldExist="false"
                  FileFilters="PMO files (*.pmo);;HDF5 files (*.h5)">
              <BriefDescription>Name of decay chain text file</BriefDescription>
              <DefaultValue>default.h5</DefaultValue>
            </File>

            <File Name="EXPORT_EDITS" Label="Region edit integral output file"
                  NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
            </File>

            <Double Name="THERMAL_POWER" Label="Power normalization (W)"
                    NumberOfRequiredValues="1">
              <DefaultValue>1.0</DefaultValue>
            </Double>

          </ItemDefinitions>
        </Group>

      </ItemDefinitions>
    </AttDef>

  </Definitions>

</SMTK_AttributeResource>
